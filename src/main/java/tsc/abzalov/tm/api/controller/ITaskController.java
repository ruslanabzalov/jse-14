package tsc.abzalov.tm.api.controller;

public interface ITaskController {

    void createTask();

    void showAllTasks();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByName();

    void editTaskById();

    void editTaskByIndex();

    void editTaskByName();

    void deleteAllTasks();

    void deleteTaskById();

    void deleteTaskByIndex();

    void deleteTaskByName();

    void startTaskById();

    void endTaskById();

    void showTasksSortedByName();

    void showTasksSortedByStartDate();

    void showTasksSortedByEndDate();

    void showTasksSortedByStatus();
    
}
