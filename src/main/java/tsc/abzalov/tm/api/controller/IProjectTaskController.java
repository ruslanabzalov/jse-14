package tsc.abzalov.tm.api.controller;

public interface IProjectTaskController {

    void addTaskToProject();

    void showProjectTasks();

    void deleteProjectTask();

    void deleteProjectWithTasks();

}
