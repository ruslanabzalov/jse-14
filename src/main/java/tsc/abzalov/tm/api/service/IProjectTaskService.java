package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    int indexOf(Task task);

    boolean hasData();

    void addTaskToProjectById(String projectId, String taskId);

    Project findProjectById(String id);

    Task findTaskById(String id);

    List<Task> findProjectTasksById(String projectId);

    void deleteProjectById(String id);

    void deleteProjectTasksById(String projectId);

    void deleteProjectTaskById(String taskId);

}
