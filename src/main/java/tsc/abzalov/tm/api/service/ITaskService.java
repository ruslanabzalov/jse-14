package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    int size();

    int indexOf(Task task);

    void createTask(String name, String description);

    List<Task> getAllTasks();

    Task getTaskById(String id);

    Task getTaskByIndex(int index);

    Task getTaskByName(String name);

    Task updateTaskById(String id, String name, String description);

    Task updateTaskByIndex(int index, String name, String description);

    Task updateTaskByName(String name, String description);

    void removeAllTasks();

    void removeTaskById(String id);

    void removeTaskByIndex(int index);

    void removeTaskByName(String name);

    Task startTaskById(String id);

    Task endTaskById(String id);

    List<Task> sortTasksByName();

    List<Task> sortTasksByStartDate();

    List<Task> sortTasksByEndDate();

    List<Task> sortTasksByStatus();
    
}
