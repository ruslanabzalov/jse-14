package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import java.util.List;

import static org.apache.commons.lang3.StringUtils.isAnyBlank;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(@NotNull IProjectRepository projectRepository, @NotNull ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public int indexOf(@NotNull Task task) {
        return taskRepository.indexOf(task);
    }

    @Override
    public boolean hasData() {
        return projectRepository.size() != 0 && taskRepository.size() != 0;
    }

    @Override
    public void addTaskToProjectById(@NotNull String projectId, @NotNull String taskId) {
        if (isAnyBlank(projectId, taskId)) return;
        taskRepository.addTaskToProjectById(projectId, taskId);
    }

    @Override
    @Nullable
    public Project findProjectById(@NotNull String id) {
        if (isBlank(id)) return null;
        return projectRepository.findProjectById(id);
    }

    @Override
    @Nullable
    public Task findTaskById(@NotNull String id) {
        if (isBlank(id)) return null;
        return taskRepository.findTaskById(id);
    }

    @Override
    @Nullable
    public List<Task> findProjectTasksById(@NotNull String projectId) {
        if (isBlank(projectId)) return null;
        return taskRepository.findProjectTasksById(projectId);
    }

    @Override
    public void deleteProjectById(@NotNull String id) {
        if (isBlank(id)) return;
        projectRepository.deleteProjectById(id);
    }

    @Override
    public void deleteProjectTasksById(@NotNull String projectId) {
        if (isBlank(projectId)) return;
        taskRepository.deleteProjectTasksById(projectId);
    }

    @Override
    public void deleteProjectTaskById(@NotNull String taskId) {
        if (isBlank(taskId)) return;
        taskRepository.deleteProjectTaskById(taskId);
    }

}
