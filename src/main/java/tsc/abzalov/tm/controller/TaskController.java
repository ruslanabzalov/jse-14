package tsc.abzalov.tm.controller;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.controller.ITaskController;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.model.Task;

import java.util.List;

import static tsc.abzalov.tm.util.InputUtil.*;

public class TaskController implements ITaskController {

    @NotNull
    private final ITaskService taskService;

    public TaskController(@NotNull ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("TASK CREATION\n");
        final String taskName = inputName();
        final String taskDescription = inputDescription();
        taskService.createTask(taskName, taskDescription);

        System.out.println("Task was successfully created.\n");
    }

    @Override
    public void showAllTasks() {
        System.out.println("ALL TASKS LIST\n");
        if (areTasksExist()) {
            final List<Task> tasks = taskService.getAllTasks();
            tasks.forEach(task -> System.out.println((taskService.indexOf(task) + 1) + ". " + task));
            System.out.println();
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

    @Override
    public void showTaskById() {
        System.out.println("FIND TASK BY ID\n");
        if (areTasksExist()) {
            final String taskId = inputId();
            System.out.println();

            final Task task = taskService.getTaskById(taskId);
            if (task == null) {
                System.out.println("Searched task was not found.\n");
                return;
            }

            System.out.println((taskService.indexOf(task) + 1) + ". " + task + "\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("FIND TASK BY INDEX\n");
        if (areTasksExist()) {
            final int taskIndex = inputIndex();
            System.out.println();

            final Task task = taskService.getTaskByIndex(taskIndex);
            if (task == null) {
                System.out.println("Searched task was not found.\n");
                return;
            }

            System.out.println((taskService.indexOf(task) + 1) + ". " + task + "\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

    @Override
    public void showTaskByName() {
        System.out.println("FIND TASK BY NAME\n");
        if (areTasksExist()) {
            final String taskName = inputName();
            System.out.println();

            final Task task = taskService.getTaskByName(taskName);
            if (task == null) {
                System.out.println("Searched task was not found.\n");
                return;
            }

            System.out.println((taskService.indexOf(task) + 1) + ". " + task + "\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

    @Override
    public void editTaskById() {
        System.out.println("EDIT TASK BY ID\n");
        if (areTasksExist()) {
            final String taskId = inputId();
            final String taskName = inputName();
            final String taskDescription = inputDescription();
            System.out.println();

            final Task task = taskService.updateTaskById(taskId, taskName, taskDescription);
            if (task == null) {
                System.out.println("Task was not updated! Please, check that task exists and try again.");
                return;
            }

            System.out.println("Task was successfully updated.");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

    @Override
    public void editTaskByIndex() {
        System.out.println("EDIT TASK BY INDEX\n");
        if (areTasksExist()) {
            final int taskIndex = inputIndex();
            final String taskName = inputName();
            final String taskDescription = inputDescription();
            System.out.println();

            final Task task = taskService.updateTaskByIndex(taskIndex, taskName, taskDescription);
            if (task == null) {
                System.out.println("Task was not updated! Please, check that task exists and try again.");
                return;
            }

            System.out.println("Task was successfully updated.");
            return;
        }
        System.out.println("Tasks list is empty.\n");
    }

    @Override
    public void editTaskByName() {
        System.out.println("EDIT TASK BY NAME\n");
        if (areTasksExist()) {
            final String taskName = inputName();
            final String taskDescription = inputDescription();
            System.out.println();

            final Task task = taskService.updateTaskByName(taskName, taskDescription);
            if (task == null) {
                System.out.println("Task was not updated! Please, check that task exists and try again.");
                return;
            }

            System.out.println("Task was successfully updated.");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

    @Override
    public void deleteAllTasks() {
        System.out.println("TASKS DELETION\n");
        if (areTasksExist()) {
            taskService.removeAllTasks();
            System.out.println("All tasks were successfully deleted.\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

    @Override
    public void deleteTaskById() {
        System.out.println("DELETE TASK BY ID\n");
        if (areTasksExist()) {
            taskService.removeTaskById(inputId());
            System.out.println("Task was successfully deleted.\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

    @Override
    public void deleteTaskByIndex() {
        System.out.println("DELETE TASK BY INDEX\n");
        if (areTasksExist()) {
            taskService.removeTaskByIndex(inputIndex());
            System.out.println("Task was successfully deleted.\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

    @Override
    public void deleteTaskByName() {
        System.out.println("DELETE TASK BY NAME\n");
        if (areTasksExist()) {
            taskService.removeTaskByName(inputName());
            System.out.println("Task was successfully deleted.\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

    @Override
    public void startTaskById() {
        System.out.println("START TASK BY ID\n");
        if (areTasksExist()) {
            final Task task = taskService.startTaskById(inputId());
            if (task == null) {
                System.out.println("Task was not started! Please, check that task exists or it has correct status.\n");
                return;
            }

            System.out.println("Task was successfully started.\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

    @Override
    public void endTaskById() {
        System.out.println("END TASK BY ID\n");
        if (areTasksExist()) {
            final Task task = taskService.endTaskById(inputId());
            if (task == null) {
                System.out.println("Task was not ended! Please, check that task exists or it has correct status.\n");
                return;
            }

            System.out.println("Task was successfully ended.\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

    @Override
    public void showTasksSortedByName() {
        System.out.println("ALL TASKS LIST SORTED BY NAME\n");
        if (areTasksExist()) {
            final List<Task> tasks = taskService.sortTasksByName();
            tasks.forEach(task -> System.out.println((taskService.indexOf(task) + 1) + ". " + task));
            System.out.println();
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

    @Override
    public void showTasksSortedByStartDate() {
        System.out.println("ALL TASKS LIST SORTED BY START DATE\n");
        if (areTasksExist()) {
            final List<Task> tasks = taskService.sortTasksByStartDate();
            tasks.forEach(task -> System.out.println((taskService.indexOf(task) + 1) + ". " + task));
            System.out.println();
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

    @Override
    public void showTasksSortedByEndDate() {
        System.out.println("ALL TASKS LIST SORTED BY END DATE\n");
        if (areTasksExist()) {
            final List<Task> tasks = taskService.sortTasksByEndDate();
            tasks.forEach(task -> System.out.println((taskService.indexOf(task) + 1) + ". " + task));
            System.out.println();
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

    @Override
    public void showTasksSortedByStatus() {
        System.out.println("ALL TASKS LIST SORTED BY STATUS\n");
        if (areTasksExist()) {
            final List<Task> foundedTasks = taskService.sortTasksByStatus();
            foundedTasks.forEach(task -> System.out.println((taskService.indexOf(task) + 1) + ". " + task));
            System.out.println();
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

    private boolean areTasksExist() {
        return taskService.size() != 0;
    }

}
